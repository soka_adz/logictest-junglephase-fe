import React, { Component, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import * as router from 'react-router-dom';
import { Container } from 'reactstrap';
// import './App.scss'
import kominfo from '../../assets/img/logo-kominfo-copy 1.png';

import {
  AppAside,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppBreadcrumb2 as AppBreadcrumb,
  AppSidebarNav2 as AppSidebarNav,
} from '@coreui/react';
// sidebar nav config
import navigation from '../../_nav';
// routes config
import routes from '../../routes';

const DefaultAside = React.lazy(() => import('./DefaultAside'));
const DefaultFooter = React.lazy(() => import('./DefaultFooter'));
const DefaultHeader = React.lazy(() => import('./DefaultHeader'));

class DefaultLayout extends Component {

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  signOut(e) {
    e.preventDefault()
    this.props.history.push('/login')
  }

  render() {
    return (
      <div className="app">
        <AppHeader fixed style={{background: '#733572'}}>
          <Suspense  fallback={this.loading()}>
            <DefaultHeader onLogout={e=>this.signOut(e)}/>
          </Suspense>
        </AppHeader>
        <div className="app-body bg-white">
          <AppSidebar fixed display="lg" style={{background: '#363740'}}>
            <AppSidebarHeader />
            <AppSidebarForm />
            <Suspense>
            <AppSidebarNav navConfig={navigation} {...this.props} router={router}/>
            </Suspense>
            <AppSidebarFooter />
            <AppSidebarMinimizer />
          </AppSidebar>
          <main className="main" >
            
              <AppBreadcrumb appRoutes={routes} router={router}/>
              
           <Container fluid>
           
           <h5 className="text-black" style={{position: 'absolute', width: '345px', height: '90px', top: '197px', 
           marginLeft: '478px', fontFamily: 'Roboto', fontStyle: 'normal', 
           fontWeight: '500', fontSize: '18px', lineHeight: '30px', 
           textAlign: 'center', letterSpacing: '2px', color: '#000000'}}>Yeyy !! 
           Kamu akan mengikuti program dari Kominfo Batch 2</h5>
                <img src= {kominfo} alt='logo kominfo' style={{
                  position: 'absolute', width: '292px', height: '244px', marginLeft: '500px', top: '293px'
                }}></img>
                <br/>
                <button className='btn btn-square text-black' style={{
                  position: 'absolute', background: '#FFB32C', borderRadius: '4px', marginTop: '20px',
                  marginLeft: '575px', right: '41%', bottom: '21.35%'
                }}>Okee</button>
             {/* <Suspense fallback={this.loading()}>
                <Switch>
                  {routes.map((route, idx) => {
                    return route.component ? (
                      <Route
                        key={idx}
                        path={route.path}
                        exact={route.exact}
                        name={route.name}
                        render={props => (
                          <route.component {...props} />
                        )} />
                    ) : (null);
                  })}
                  <Redirect from="/" to="/dashboard" />
                </Switch>
                </Suspense> */}
              </Container>
                </main> 
          <AppAside fixed>
            <Suspense fallback={this.loading()}>
              <DefaultAside />
            </Suspense>
          </AppAside>
        </div>
        <AppFooter>
          <Suspense fallback={this.loading()}>
            <DefaultFooter />
          </Suspense>
        </AppFooter>
      </div>
    );
  }
}

export default DefaultLayout;
